package com.atishay.test 
{
	import com.atishay.test.interfaces.IProp;
	import QuickB2.objects.tangibles.qb2Body;
	import QuickB2.stock.qb2Stock;
	import As3Math.geo2d.amPoint2d;
	
	/**
	 * ...
	 * @author atjain
	 */
	public class Level extends qb2Body
	{
		public var top:Number;
		public var bottom:Number;
		public var left:Number;
		public var right:Number;
		public static var instance:Level = new Level();
		public function init(wallWidth:int = 50):Level
		{
			this.removeAllObjects();
			addObject(qb2Stock.newRectShape(new amPoint2d( left - wallWidth/2, (top + bottom + wallWidth)/2), wallWidth, bottom - top + wallWidth, 0, 0));
			addObject(qb2Stock.newRectShape(new amPoint2d( (left + right+ wallWidth)/2,  bottom + wallWidth/2 ), right - left + wallWidth, wallWidth, 0, 0));
			addObject(qb2Stock.newRectShape(new amPoint2d( right + wallWidth/2, (top + bottom - wallWidth)/2), wallWidth, bottom - top + wallWidth, 0, 0));
			addObject(qb2Stock.newRectShape(new amPoint2d(  (left + right - wallWidth)/2, top - wallWidth/2), right - left + wallWidth, wallWidth, 0, 0));
			return this;
		}
	}

}