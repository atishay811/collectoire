package com.atishay.test.ui
{
	import com.flashandmath.dg.textures.*;
	import flash.display.*;
	import flash.events.*;
	import flash.filters.*;
	import flash.text.*;
	import com.atishay.test.Main;
	import com.atishay.test.events.CollectEvent;
	
	/**
	 * ...
	 * @author atjain
	 */
	public class uiScreen extends Sprite
	{
		public static var instance:uiScreen;
		//private var woodFrame:WoodTexture = null;
		private var repeatable:Sprite = new Sprite();
		private var _score:int = 0;
		private var scoreBoard:TextField = new TextField();
		
		[Embed(source = "../../../../assets/Stencilia.ttf", fontName = "Stencilia-A",  mimeType = "application/x-font",  embedAsCFF="false",  unicodeRange='U+0030-U+0039')]
		public static const Stencilia:Class;
		
		[Embed(source="../../../../assets/scoreboard.png")]
        private static var sbImage:Class;
		
		[Embed(source="../../../../assets/left.png")]
        private static var leftImage:Class;
		private static var leftBmp:Bitmap = new leftImage();
		
		[Embed(source="../../../../assets/right.png")]
        private static var rightImage:Class;
		private static var rightBmp:Bitmap = new rightImage();
		
		[Embed(source="../../../../assets/top.png")]
        private static var topImage:Class;
		private static var topBmp:Bitmap = new topImage();
		
		[Embed(source="../../../../assets/bottom.png")]
        private static var bottomImage:Class;
		private static var bottomBmp:Bitmap = new bottomImage();
		
		[Embed(source="../../../../assets/corner.png")]
        private static var cornerImage:Class;
		
		public function uiScreen()
		{
			super();
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
			prepareRepeatable();
			instance = this;
			Main.GameSprite.addEventListener(CollectEvent.COLLECT_SUCCESS, handleCollection);
		}
		
		private function handleCollection(e:CollectEvent):void
		{
			if (e.type == CollectEvent.COLLECT_SUCCESS)
			{
				_score += e.object.points;
				scoreBoard.text = ("0000" + _score).substr(-4, 4);
			}
			e.stopImmediatePropagation();
		}
		
		private function addedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			parent.stage.addEventListener(Event.RESIZE, resizeUI);
			resizeUI(null);
		}
		
		private function resizeUI(e:Event):void
		{
			this.x = 0;
			this.y = 0;
			this.width = parent.width;
			this.height = parent.height;
			this.scaleX = 1;
			this.scaleY = 1;
			drawBorder();
		}
		private var cachedFrame:Sprite;
		private function drawBorder():void
		{
			/*var baseFrame:Sprite = new Sprite();
			baseFrame.graphics.beginFill(0xFF0000, 1);
			baseFrame.graphics.lineStyle(0);
			baseFrame.graphics.drawRect(0, 5, 5, stage.stageHeight - 5);
			baseFrame.graphics.drawRect(0, 0, stage.stageWidth, 5);
			baseFrame.graphics.drawRect(stage.stageWidth - 5, 5, 5, stage.stageHeight - 5);
			baseFrame.graphics.drawRect(5, stage.stageHeight - 60, stage.stageWidth - 10, 60); 
			
			//ScoreBoard
			baseFrame.graphics.drawRoundRect(stage.stageWidth - 140, 10, 130, 50, 50, 50);
			baseFrame.graphics.drawCircle(stage.stageWidth - 110, 18, 2);
			baseFrame.graphics.drawCircle(stage.stageWidth - 40, 18, 2);
			
			baseFrame.graphics.endFill();
			if (woodFrame)
				removeChild(woodFrame);
			woodFrame = new WoodTexture(baseFrame);
			woodFrame.colorList = new <uint>[0x7a2102, 0x872707, 0x932f03, 0xa83d18, 0x6a1901, 0x560e01];
			woodFrame.generate();
			woodFrame.filters = [new BevelFilter(2)];
			woodFrame.x = 0;
			woodFrame.y = 0;
			addChild(woodFrame);
			woodFrame.cacheAsBitmap = true;*/
			if (cachedFrame)
				removeChild(cachedFrame);
			cachedFrame = new Sprite();
			addChild(cachedFrame);
			cachedFrame.x = 0;
			cachedFrame.y = 0;
			
			cachedFrame.graphics.beginBitmapFill(leftBmp.bitmapData);
			cachedFrame.graphics.drawRect(0, 5, 5, stage.stageHeight - 10);
			cachedFrame.graphics.beginBitmapFill(rightBmp.bitmapData);
			cachedFrame.graphics.drawRect(stage.stageWidth - 5, 5, 5, stage.stageHeight - 10);
			cachedFrame.graphics.beginBitmapFill(topBmp.bitmapData);
			cachedFrame.graphics.drawRect(5, 0, stage.stageWidth - 10, 5);
			cachedFrame.graphics.beginBitmapFill(bottomBmp.bitmapData);
			cachedFrame.graphics.drawRect(5, stage.stageHeight - 5, stage.stageWidth - 10, 5);
			
			var corner:Bitmap = new cornerImage();
			corner.x = corner.y = 0;
			cachedFrame.addChild(corner);
			
			corner = new cornerImage();
			corner.x = stage.stageWidth;
			corner.y = 0;
			corner.rotationZ = 90;
			cachedFrame.addChild(corner);
			
			corner = new cornerImage();
			corner.x = 0;
			corner.y = stage.stageHeight;
			corner.rotationZ = -90;
			cachedFrame.addChild(corner);
			
			corner = new cornerImage();
			corner.x = stage.stageWidth;
			corner.y = stage.stageHeight;
			corner.rotationZ = 180;
			cachedFrame.addChild(corner);
			
			cachedFrame.cacheAsBitmap = true;
			repeatable.x = stage.stageWidth - 140;
			repeatable.y = 0;
			setChildIndex(repeatable, numChildren - 1);
		}
		
		private function prepareRepeatable():void
		{
			/*repeatable.graphics.beginFill(0);
			repeatable.graphics.lineStyle(1,0x111111);
			repeatable.graphics.drawCircle(15, 18, 2);
			repeatable.graphics.drawCircle(85, 18, 2);
			repeatable.graphics.lineStyle(2);
			repeatable.graphics.moveTo(15, 5);
			repeatable.graphics.lineTo(15, 18);
			repeatable.graphics.moveTo(85, 5);
			repeatable.graphics.lineTo(85, 18);
			repeatable.graphics.endFill();*/
			var sb:Bitmap = new sbImage();
			sb.y = 5;
			sb.x = 0;
			repeatable.addChild(sb);
			//ScoreBoard
			scoreBoard.x = 15;
			scoreBoard.y = 18;
			scoreBoard.text = ("0000" + _score).substr(-4, 4);
			var format:TextFormat	      = new TextFormat();
			format.font 				  = "Stencilia-A";
			format.bold = false;
			format.italic = false;
			format.color				  = 0xF2F2F2;
			format.size					  = 30;
			format.align 				  = "center";
			
			scoreBoard.antiAliasType         = AntiAliasType.ADVANCED;
			scoreBoard.defaultTextFormat     = format;
			scoreBoard.setTextFormat(format);
			scoreBoard.type					 = TextFieldType.DYNAMIC;
			scoreBoard.alpha = 0.8;
			repeatable.addChild(scoreBoard);
			scoreBoard.selectable = false;
			addChild(repeatable);
			
		}
	}

}