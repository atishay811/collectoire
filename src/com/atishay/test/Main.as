package com.atishay.test
{
	import As3Math.geo2d.*;
	import com.atishay.test.props.*;
	import com.atishay.test.props.params.*;
	import com.atishay.test.props.utils.*;
	import com.atishay.test.ui.*;
	import com.flashandmath.dg.textures.*;
	import flash.display.*;
	import flash.events.*;
	import flash.ui.*;
	import flash.utils.ByteArray;
	import QuickB2.misc.acting.qb2FlashSpriteActor;
	import QuickB2.misc.acting.qb2IActorContainer;
	import QuickB2.objects.tangibles.*;
	import QuickB2.stock.*;
	import flash.desktop.NativeApplication;
	import by.blooddy.crypto.image.PNG24Encoder;
	import flash.filesystem.File;
	import surrender.srVectorGraphics2d;
	import com.atishay.test.utils.LevelSetter;
	
	//import QuickB2.objects.*;
	
	/**
	 * ...
	 * @author atjain
	 */
	[Frame(factoryClass="com.atishay.test.utils.Preloader")]
	
	public class Main extends Sprite
	{
		[Embed(source="../../../assets/floor.jpg")]
		private static var floorImage:Class;
		public static var floor:Bitmap = new floorImage();
		
		public static var singleton:Main;
		public static var GameSprite:qb2FlashSpriteActor = new qb2FlashSpriteActor();
		public static var world:qb2World;
		private static var drawing:Drawing;
		
		public function Main():void
		{
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		private function onZoom(e:TransformGestureEvent):void
		{
			drawing.closedrawLoop(true);
			GameSprite.scaleX *= (e.scaleX + e.scaleY) / 2;
			GameSprite.scaleY *= (e.scaleX + e.scaleY) / 2;
		}
		
		private function onPan(e:TransformGestureEvent):void
		{
			drawing.closedrawLoop(true);
			GameSprite.x += e.offsetX;
			GameSprite.y += e.offsetY;
		}
		
		private function addedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			singleton = this;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.addEventListener(Event.DEACTIVATE, deactivate);
			// touch or gesture?
			Multitouch.inputMode = MultitouchInputMode.GESTURE;
			
			addEventListener(TransformGestureEvent.GESTURE_ZOOM, onZoom);
			addEventListener(TransformGestureEvent.GESTURE_PAN , onPan); 
			
			//Adding Overall Background
			var bg:Sprite = new Sprite();
			bg.graphics.beginBitmapFill(floor.bitmapData);
			bg.graphics.drawRect(0, 0, Math.max(stage.stageWidth, stage.stageHeight), Math.max(stage.stageWidth, stage.stageHeight));
			bg.graphics.endFill();
			addChild(bg);
			//var woodenBg:WoodTexture = new WoodTexture(bg);
			//woodenBg.generate();
			//woodenBg.cacheAsBitmap = true;
			//addChild(woodenBg);
			
			//Adding Game Sprite
			addChild(GameSprite);
			//GameSprite.scaleX = 0.75;
			//GameSprite.scaleY = 0.75;
			GameSprite.x = stage.stageWidth/2;
			GameSprite.y = stage.stageHeight/2;
			var ui:uiScreen = new uiScreen();
			addChild(ui);
			
			/*var bitmapData:BitmapData = new BitmapData(1000, 1000);//(stage.stageWidth, stage.stageHeight);
			   bitmapData.draw(woodenBg.stage);
			   var bitmap : Bitmap = new Bitmap(bitmapData);
			   var jpg:PNG24Encoder = new PNG24Encoder();
			   var ba:ByteArray = PNG24Encoder.encode(bitmapData);
			
			   var file:File = File.desktopDirectory;
			 file.save(ba, "bg.png");*/
			
			//GameSprite.x = 100;
			//GameSprite.y = 10;
			
			world = new qb2World(); 
			var debugDrawSprite:Sprite = GameSprite;
			world.debugDrawGraphics = new srVectorGraphics2d(debugDrawSprite.graphics);
			world.gravity = new amVector2d(0, 5);
			world.actor = new qb2FlashSpriteActor();
			GameSprite.addChild(world.actor as qb2FlashSpriteActor);
			
			world.start();
			new PropFactory(world);
			world.addObject(drawing = new Drawing());
			world.restitution = .8;
			
			Props.register();
			LevelSetter.instance.init().prepareLevel(1);
		}
		
		private function deactivate(e:Event):void
		{
			// auto-close
			NativeApplication.nativeApplication.exit();
		}
	
	}

}