package com.atishay.test.events 
{
	import com.atishay.test.interfaces.IDroppable;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author atjain
	 */
	public class CollectEvent extends Event 
	{
		public static const COLLECT_SUCCESS:String = "collectSuccess";
		public static const COLLECT_FAILED:String = "collectFailed";
		public var object:IDroppable = null;
		private static var cached_success:CollectEvent = new CollectEvent(COLLECT_SUCCESS);
		private static var cached_failed:CollectEvent = new CollectEvent(COLLECT_FAILED);
		
		public static function getCached(type:String, object:IDroppable = null):CollectEvent
		{
			switch(type)
			{
				case COLLECT_SUCCESS:
					cached_success.object = object;
					return cached_success;
				case COLLECT_FAILED:
					cached_failed.object = object;
					return cached_failed;
			}
			return null;
		}
		public function CollectEvent(type:String, object:IDroppable = null, bubbles:Boolean = true, cancelable:Boolean = false) 
		{ 
			this.object = object;
			super(type, bubbles, cancelable);
		} 
		
		public override function clone():Event 
		{ 
			return new CollectEvent(type, object, bubbles, cancelable);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("CollectEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}