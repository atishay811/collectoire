package com.atishay.test
{
	import As3Math.geo2d.*;
	import com.atishay.test.utils.*;
	import flash.display.*;
	import flash.events.*;
	import flash.filters.*;
	import QuickB2.events.*;
	import QuickB2.misc.acting.qb2FlashSpriteActor;
	import QuickB2.objects.*;
	import QuickB2.objects.tangibles.*;
	import QuickB2.stock.*;
	
	public class Drawing extends ObjectSet
	{
		[Embed(source="../../../assets/pattern.jpg")]
		private static var imagePattern:Class;
		public static var bmp:Bitmap = new imagePattern();
		
		private static const MIN_DISTANCE:Number = 80;
		private static const LINE_THICKNESS:Number = 10;
		private var actorSprite:Sprite = new Sprite();
		private var currPolyline:qb2Body;
		public function Drawing()
		{
			this.restitution = .5;
			this.actor = new qb2FlashSpriteActor();
			(actor as qb2FlashSpriteActor).addChild(actorSprite);
			//actorSprite.filters = [new GlowFilter(0x0)];
			
			actorSprite.graphics.beginBitmapFill(bmp.bitmapData);
			actorSprite.graphics.lineStyle(LINE_THICKNESS);
			
			actorSprite.graphics.lineBitmapStyle(bmp.bitmapData);
			stage.addEventListener(MouseEvent.MOUSE_DOWN, toggleDragging);
			addEventListener(qb2UpdateEvent.POST_UPDATE, updateLines);
		}
		
		private const pointStack:Vector.<amPoint2d> = new Vector.<amPoint2d>();
		
		private function updateLines(evt:qb2UpdateEvent):void
		{
			if (!mouseDown)
				return;
			
			var currPoint:amPoint2d = new amPoint2d(TouchUtils.mouseX, TouchUtils.mouseY);
			if (currPoint.distanceTo(lastPoint) >= MIN_DISTANCE)
			{
				pointStack.push(currPoint);
				
				flushPolyline();
				
				lastPoint = currPoint;
			}
		}
		
		private function flushPolyline():void
		{
			
			if (currPolyline == null)
			{
				currPolyline = new qb2Body();
				addObject(currPolyline);
				if (pointStack.length == 1)
				{
					actorSprite.graphics.lineStyle(LINE_THICKNESS, 0, 0);
					actorSprite.graphics.drawCircle(pointStack[0].x, pointStack[0].y, LINE_THICKNESS / 2);
					actorSprite.graphics.lineStyle(LINE_THICKNESS);
					actorSprite.graphics.lineBitmapStyle(bmp.bitmapData);
				}
				currPolyline.addObject(qb2Stock.newCircleShape(pointStack[0], LINE_THICKNESS / 2));
			}
			
			if (pointStack.length != 1)
			{
				var point1:amPoint2d = pointStack[pointStack.length - 2];
				var point2:amPoint2d = pointStack[pointStack.length - 1];
				var vec:amVector2d = point2.minus(point1);
				var rect:qb2PolygonShape = qb2Stock.newRectShape(point1.midwayPoint(point2), LINE_THICKNESS, vec.length, 0, vec.angle);
				rect.position.subtract(currPolyline.position);
				currPolyline.addObject(rect);
				actorSprite.graphics.moveTo(point1.x, point1.y);
				actorSprite.graphics.lineTo(point2.x, point2.y);
			}
		}
		
		private var mouseDown:Boolean = false;
		private var lastPoint:amPoint2d = null;
		private var worldDragSprite:InteractiveObject = null;
		
		private function toggleDragging(evt:MouseEvent):void
		{
			var testPoint:amPoint2d = new amPoint2d(TouchUtils.mouseX, TouchUtils.mouseY);
			
			//--- Don't start drawing if this mouse down will invoke a drag of one of the circles.
			if (evt.type == MouseEvent.MOUSE_DOWN)
			{
				for (var i:int = 0; i < numObjects; i++)
				{
					var ithObject:qb2Object = getObjectAt(i);
					if (!(ithObject is qb2CircleShape))
						continue;
					
					if ((ithObject as qb2CircleShape).testPoint(testPoint))
					{
						return;
					}
				}
			}
			
			lastPoint = testPoint;
			
			if (evt.type == MouseEvent.MOUSE_DOWN)
			{
				mouseDown = true;
				stage.addEventListener(MouseEvent.MOUSE_UP, toggleDragging);
				
				pointStack.push(lastPoint);
				
				worldDragSprite = world.debugDragSource;
				world.debugDragSource = null;
			}
			else
			{
				if (pointStack.length == 1)
					flushPolyline();
				else
				{
					currPolyline.addObject(qb2Stock.newCircleShape(pointStack[pointStack.length - 1], LINE_THICKNESS / 2));
				}
				closedrawLoop();
				
			}
		}
		
		public function closedrawLoop(cleanUp:Boolean = false):void
		{
			if (!mouseDown)
				return;
			mouseDown = false;
			stage.removeEventListener(MouseEvent.MOUSE_UP, toggleDragging);
				
			lastPoint = null;
			pointStack.length = 0;
			if (cleanUp && currPolyline && this.getObjectIndex(currPolyline) != -1)
				removeObject(currPolyline);
				
			currPolyline = null;
				
			world.debugDragSource = worldDragSprite;
		}
		
		public function emptyView():void
		{
			removeAllObjects();
			actorSprite.removeChildren();
		}
		
		protected override function addedOrRemoved(evt:qb2ContainerEvent):void
		{
			if (evt.type == qb2ContainerEvent.ADDED_TO_WORLD)
			{
				addEventListener(qb2UpdateEvent.POST_UPDATE, updateLines);
				stage.addEventListener(MouseEvent.MOUSE_DOWN, toggleDragging);
			}
			else
			{
				removeEventListener(qb2UpdateEvent.POST_UPDATE, updateLines);
				stage.removeEventListener(MouseEvent.MOUSE_DOWN, toggleDragging);
			}
		}
	
	}

}