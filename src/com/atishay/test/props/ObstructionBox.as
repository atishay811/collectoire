package com.atishay.test.props 
{
	import As3Math.geo2d.amPoint2d;
	import com.atishay.test.interfaces.*;
	import com.atishay.test.props.params.*;
	import QuickB2.misc.acting.qb2FlashSpriteActor;
	import QuickB2.objects.tangibles.*;
	import QuickB2.stock.qb2Stock;
	import com.atishay.test.Drawing;
	import flash.filters.GlowFilter;
	
	/**
	 * ...
	 * @author atjain
	 */
	public class ObstructionBox extends qb2Body implements IProp 
	{
		private var _title:String;
		
		public function init(params:Object):Boolean
		{
			var initParams:ObstructionBoxParams;
			if (params is ObstructionBoxParams)
				initParams = params as ObstructionBoxParams;
			else
				initParams = ObstructionBoxParams.instance.resetViaXml(params as XML);
			this.rotation = initParams.rotation;
			addObject(qb2Stock.newRectShape(new amPoint2d(0, 0), initParams.width, initParams.height, 0, 0));	
			this.position = initParams.position
			var actorSprite:qb2FlashSpriteActor = new qb2FlashSpriteActor();
			//actorSprite.filters = [new GlowFilter(0x0)];
			actorSprite.graphics.lineStyle(2);
			actorSprite.graphics.beginBitmapFill(Drawing.bmp.bitmapData);
			actorSprite.graphics.drawRect(-initParams.width/2, -initParams.height/2, initParams.width, initParams.height);
			this.actor = actorSprite;
			return true;
			
		}
		public function cleanUp():void
		{
			
		}
		public function get title():String 
		{
			return _title;
		}
		
		public function set title(value:String):void 
		{
			_title = value;
		}
		
	}

}