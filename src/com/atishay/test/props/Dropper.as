package com.atishay.test.props
{
	import com.atishay.test.*;
	import com.atishay.test.interfaces.*;
	import com.atishay.test.props.*;
	import com.atishay.test.props.params.*;
	import com.atishay.test.props.utils.DropSpecs;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import QuickB2.events.*;
	import QuickB2.misc.acting.qb2FlashSpriteActor;
	import QuickB2.objects.tangibles.*;
	
	/**
	 * ...
	 * @author atjain
	 */
	public class Dropper extends qb2Body implements IProp
	{
		public var World:qb2World;
		public var genStep:Number = 0;
		private var elapsed:Number = 0;
		private var _title:String;
		private var _useJitter:Boolean = true;
		private var _frequencyDict:Vector.<DropSpecs>;
		private var totalFrequency:uint = 0;
		private var _useGenerationJitter:Boolean = true;		
		private var searchValue:uint = 0;
		private var reuseBmp:Class = null;
		
		public function Dropper()
		{
			World = Main.world;
		}
		
		public function init(params:Object):Boolean
		{
			var initParams:DropperParams;
			if(params is DropperParams)
				initParams = params as DropperParams;
			else
				initParams = DropperParams.instance.resetViaXml(params as XML);
			position = initParams.position;
			genStep = initParams.genStep;
			
			_useJitter = initParams.useJitter;
			_frequencyDict = initParams.frequencyDict;
			totalFrequency = initParams.totalFrequency;
			_useGenerationJitter = initParams.useGenerationJitter;
			if (initParams.useJitter)
			{
				genStep *= 2;
				elapsed = Math.random() * genStep;
			}
			World.addEventListener(qb2UpdateEvent.POST_UPDATE, handleWorldUpdate, false);
			if (initParams.actorBmp && (reuseBmp == null || initParams.actorBmp != reuseBmp))
				{
					var actor:Bitmap = new initParams.actorBmp();
					actor.smoothing = true;
					var act:Sprite = new Sprite();
					act.addChild(actor);
					actor.x = -actor.width / 2;
					actor.y = -actor.width / 2;
					this.actor = new qb2FlashSpriteActor();
					(this.actor as qb2FlashSpriteActor).addChild(actor);
					(this.actor as qb2FlashSpriteActor).setPosition(position);
					Main.GameSprite.addActor(this.actor);
					reuseBmp = initParams.actorBmp;
				}
				else if (initParams.actorBmp == reuseBmp)
				{
					Main.GameSprite.addChild(this.actor as qb2FlashSpriteActor);
				}
			return false;
		}
		
		private function handleWorldUpdate(e:qb2UpdateEvent):void
		{
			if (genStep == 0)
			{
				World.removeEventListener(qb2UpdateEvent.POST_UPDATE, handleWorldUpdate, false);
				return;
			}
			elapsed += (e.object as qb2World).lastTimeStep;
			if (int(elapsed / genStep) == 0)
				return;
			for (var j:int = 1; j <= elapsed / genStep; j++)
			{
				createProp();
			}
			if (_useJitter)
				elapsed = Math.random() * genStep;
			else
				elapsed %= genStep;
		}
		
		public function get title():String
		{
			return _title;
		}
		
		public function set title(value:String):void
		{
			_title = value;
		}
		
		public function cleanUp():void
		{
			//if (actor)
				//actor.parent.removeChild(actor);
			World.removeEventListener(qb2UpdateEvent.POST_UPDATE, handleWorldUpdate, false);
		}
		
		private function createProp():void
		{
			try
			{
				var prop:DropSpecs = null;
				prop = selectProp();
				
				if (prop.params == null)
				{
					prop.params = PropFactory.singleton.objectTypes[prop.title].paramClass.instance.defaultValues();
				}
				//Force Dropper Position for Drop
				prop.params.position = this.position.clone();
				Main.world.addObject(PropFactory.singleton.create(prop.title, prop.params));
			}
			catch (e:Error)
			{
				trace("Dropper :: Unable to create Prop");
			}
		
		}

		
		private function selectProp():DropSpecs
		{
			if (_frequencyDict.length == 1)
				return _frequencyDict[0];
			
			if (_useGenerationJitter)
				searchValue = Math.random() * totalFrequency + 1;
			else
			{
				searchValue %= totalFrequency;
				searchValue++;
			}
			
			//Binary Search for the random number
			var b:int = 0;
			var t:int = _frequencyDict.length - 1;
			
			var index:uint = 0;
			while (b <= t)
			{
				index = (b + t) / 2;
				
				if (_frequencyDict[index].locationNum == searchValue)
				{
					return _frequencyDict[index];
				}
				else if (_frequencyDict[index].locationNum > searchValue)
				{
					t = index - 1;
				}
				else
				{
					b = index + 1;
				}
			}
			if (_frequencyDict[index].locationNum < searchValue)
				index++;
			return _frequencyDict[index];
		}
	}

}