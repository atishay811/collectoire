package com.atishay.test.props.droppables
{
	import As3Math.geo2d.*;
	import com.atishay.test.interfaces.*;
	import com.atishay.test.props.params.RigidBallParams;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import QuickB2.misc.acting.qb2FlashSpriteActor;
	import QuickB2.objects.tangibles.*;
	
	/**
	 * ...
	 * @author atjain
	 */
	public class RigidBall extends qb2CircleShape implements IDroppable
	{
		private var _title:String;
		
		public function RigidBall()
		{
			super();
		}
		
		public function init(params:Object):Boolean
		{
			var initParams:RigidBallParams = params as RigidBallParams;
			position = initParams.position;
			radius = initParams.radius;
			mass = initParams.mass;
			hasFixedRotation = initParams.hasFixedrotation;
			if (initParams.actorBmp &&( reuseBmp == null || initParams.actorBmp != reuseBmp))
			{
				var actor:Bitmap = new initParams.actorBmp();
				reuseBmp = initParams.actorBmp;
				actor.smoothing = false;
				actor.x = -actor.width / 2;
				actor.y = -actor.width / 2;
				this.actor = new qb2FlashSpriteActor();
				(this.actor as qb2FlashSpriteActor).setPosition(position);
				(this.actor as qb2FlashSpriteActor).addChild(actor);
			}
			return true;
		}
		
		public function get title():String
		{
			return _title;
		}
		
		public function set title(value:String):void
		{
			_title = value;
		}
		
		public function cleanUp():void
		{
			linearVelocity = new amVector2d();
			angularVelocity = 0;
		}
		
		public function get points():int
		{
			return 2;
		}
		
		public static function get paramClass():Class
		{
			return RigidBallParams;
		}
		
		private var reuseBmp:Class = null;
	}

}