package com.atishay.test.props.droppables
{
	import As3Math.geo2d.*;
	import com.atishay.test.interfaces.*;
	import com.atishay.test.props.params.*;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import QuickB2.misc.acting.qb2FlashSpriteActor;
	import QuickB2.objects.tangibles.*;
	
	/**
	 * ...
	 * @author atjain
	 */
	public class RigidBox extends qb2PolygonShape implements IDroppable
	{
		private var _title:String;
		
		public function RigidBox()
		{
			super();
		
		}
		
		public function init(params:Object):Boolean
		{
			try
			{
				var initParams:RigidBoxParams = params as RigidBoxParams;
				const verts:Vector.<amPoint2d> = new Vector.<amPoint2d>(4, true);
				verts[0] = new amPoint2d(initParams.position.x - initParams.width / 2, initParams.position.y - initParams.height / 2);
				verts[1] = new amPoint2d(initParams.position.x + initParams.width / 2, initParams.position.y - initParams.height / 2);
				verts[2] = new amPoint2d(initParams.position.x + initParams.width / 2, initParams.position.y + initParams.height / 2);
				verts[3] = new amPoint2d(initParams.position.x - initParams.width / 2, initParams.position.y + initParams.height / 2);
				set(verts, initParams.position);
				rotation = initParams.initRotation;
				mass = initParams.mass;
				if (initParams.actorBmp && (reuseBmp == null || initParams.actorBmp != reuseBmp))
				{
					var actor:Bitmap = new initParams.actorBmp();
					reuseBmp = initParams.actorBmp;
					actor.smoothing = true;
					actor.x = -actor.width / 2;
					actor.y = -actor.width / 2;
					this.actor = new qb2FlashSpriteActor();
					(this.actor as qb2FlashSpriteActor).setPosition(position);
					(this.actor as qb2FlashSpriteActor).addChild(actor);
				}
			}
			catch (e:Error)
			{
				trace("RigidBox :: Missing Param");
			}
			return true;
		}
		
		public function cleanUp():void
		{
			linearVelocity = new amVector2d();
			angularVelocity = 0;
			rotation = 0;
		}
		
		public function get title():String
		{
			return _title;
		}
		
		public function set title(value:String):void
		{
			_title = value;
		}
		
		public function get points():int
		{
			return 3;
		}
		
		public static function get paramClass():Class
		{
			return RigidBoxParams;
		}
		private var reuseBmp:Class = null;
	}

}