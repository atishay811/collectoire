package com.atishay.test.props
{
	import As3Math.geo2d.*;
	import com.atishay.test.*;
	import com.atishay.test.events.*;
	import com.atishay.test.interfaces.*;
	import com.atishay.test.props.params.*;
	import flash.display.*;
	import flash.events.*;
	import QuickB2.events.*;
	import QuickB2.misc.acting.*;
	import QuickB2.objects.tangibles.*;
	import QuickB2.stock.*;
	
	/**
	 * ...
	 * @author atjain
	 */
	public class MagicHat extends qb2Body implements IProp
	{
		private var finishShape:qb2Shape;
		private var _title:String;
		private var reuseBmp:Class = null;
		
		public function MagicHat()
		{
			super();
			addObject(qb2Stock.newRectShape(new amPoint2d(7, 30), 10, 10));
			addObject(qb2Stock.newRectShape(new amPoint2d(22, 70), 10, 55));
			addObject(qb2Stock.newRectShape(new amPoint2d(48, 100), 60, 10, 0));
			addObject(qb2Stock.newRectShape(new amPoint2d(78, 70), 10, 55));
			addObject(qb2Stock.newRectShape(new amPoint2d(93, 30), 10, 10));
			finishShape = qb2Stock.newCircleShape(new amPoint2d(50, 50), 20);
			addObject(finishShape);
		}
		
		public function init(params:Object):Boolean
		{
			var initParams:MagicHatParams;
			if (params is MagicHatParams)
				initParams = params as MagicHatParams;
			else
				initParams = MagicHatParams.instance.resetViaXml(params as XML);
			this.position = initParams.position;
			this.rotation = initParams.rotation;
			if (initParams.actorBmp && (reuseBmp == null || initParams.actorBmp != reuseBmp))
			{
				var actor:Bitmap = new initParams.actorBmp();
				reuseBmp = initParams.actorBmp;
				this.actor = new qb2FlashBitmapActor();
				(this.actor as qb2FlashBitmapActor).setPosition(position);
				(this.actor as qb2FlashBitmapActor).bitmapData = actor.bitmapData;
				(this.actor as qb2FlashBitmapActor).rotation = initParams.rotation;
				if (initParams.rotation != 0)
					(this.actor as qb2FlashBitmapActor).smoothing = true;
				else
				(this.actor as qb2FlashBitmapActor).smoothing = false;
			}
			addEventListener(qb2ContactEvent.CONTACT_STARTED, contactStarted, false);
			return true;
		}
		
		public function cleanUp():void
		{
			removeEventListener(qb2ContactEvent.CONTACT_STARTED, contactStarted);
		}
		
		private function contactStarted(evt:qb2ContactEvent):void
		{
			if (evt.localShape == finishShape)
			{
				if (evt.otherObject is IDroppable)
				{
					(actor as IEventDispatcher).dispatchEvent(CollectEvent.getCached(CollectEvent.COLLECT_SUCCESS, (evt.otherObject as IDroppable)));
					PropFactory.singleton.remove(evt.otherObject as IDroppable);
				}
			}
		}
		
		public function get title():String
		{
			return _title;
		}
		
		public function set title(value:String):void
		{
			_title = value;
		}
	}

}