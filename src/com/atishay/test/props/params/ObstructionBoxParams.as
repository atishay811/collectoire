package com.atishay.test.props.params 
{
	import As3Math.geo2d.amPoint2d;
	/**
	 * ...
	 * @author atjain
	 */
	public class ObstructionBoxParams 
	{
		public static const instance:ObstructionBoxParams = new ObstructionBoxParams();
		public var position:amPoint2d = new amPoint2d();
		public var rotation:Number;
		public var width:Number = 0;
		public var height:Number = 0;
		
		public function resetViaXml(xml:XML):ObstructionBoxParams
		{
			var x:Number = Number(xml.Position.X[0]);
			var y:Number = Number(xml.Position.Y[0]);
			var rotation:Number = Number(xml.Rotation[0]);
			var width:Number = Number(xml.Scale.X[0]) * 100;
			var height:Number = Number(xml.Scale.Y[0]) * 100;
			return this.reset(x,y,width,height,rotation);
		}
		
		public function reset(x:Number, y:Number, width:Number = 100, height:Number = 100, rotation:Number = 0):ObstructionBoxParams
		{
			this.width = width;
			this.height = height;
			this.position = new amPoint2d(x, y);
			this.rotation = rotation;
			return this;
		}		
	}

}