package com.atishay.test.props.params 
{
	import As3Math.geo2d.amPoint2d;
	/**
	 * ...
	 * @author ...
	 */
	public class MagicHatParams 
	{
		public static const instance:MagicHatParams = new MagicHatParams();
		public var position:amPoint2d = new amPoint2d();
		public var rotation:Number;
		
		public function resetViaXml(xml:XML):MagicHatParams
		{
			var x:Number = Number(xml.Position.X[0]);
			var y:Number = Number(xml.Position.Y[0]);
			var rotation:Number = Number(xml.Rotation[0]);
			return this.reset(x,y,rotation);
		}
		
		[Embed(source="../../../../../assets/hat.png")]
        private static var imageHat:Class;
		public var actorBmp:Class = imageHat;
		public function reset(x:Number, y:Number, rotation:Number = 0, actorBmp:Class = null):MagicHatParams
		{
			this.position = new amPoint2d(x, y);
			this.rotation = rotation;
			if(actorBmp == null)
				this.actorBmp = imageHat;
			else
				this.actorBmp = actorBmp;
			return this;
		}
		
	}

}