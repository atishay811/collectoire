package com.atishay.test.props.params
{
	import As3Math.geo2d.*;
	import com.atishay.test.interfaces.*;
	
	/**
	 * ...
	 * @author atjain
	 */
	public class RigidBoxParams implements IDroppableParams
	{
		//Use instance for caching purposes.
		public static var instance:RigidBoxParams = new RigidBoxParams();
		
		[Embed(source="../../../../../assets/box.png")]
		private static var imageBox:Class;
		public var actorBmp:Class = imageBox;
		private var _position:amPoint2d;
		public var width:Number;
		public var height:Number;
		public var mass:Number;
		public var initRotation:Number;
		
		public function reset(x:Number, y:Number, width:Number = 10, height:Number = 10, mass:Number = 0, initRotation:Number = 0, actorBmp:Class = null):RigidBoxParams
		{
			this.position = new amPoint2d(x, y);
			this.width = width;
			this.height = height;
			this.initRotation = initRotation;
			this.mass = mass;
			if(actorBmp == null)
				this.actorBmp = imageBox;
			else
				this.actorBmp = actorBmp;
			return this;
		}
		
		public function defaultValues():IDroppableParams
		{
			width = 20;
			height = 20;
			mass = 1;
			initRotation = 0;
			actorBmp = imageBox;
			return this;
		}
		
		public function get position():amPoint2d
		{
			return _position;
		}
		
		public function set position(value:amPoint2d):void
		{
			_position = value;
		}
	}

}