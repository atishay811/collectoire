package com.atishay.test.props.params
{
	import As3Math.geo2d.amPoint2d;
	import com.atishay.test.props.utils.DropSpecs;
	import flash.utils.Dictionary;
	import QuickB2.objects.tangibles.qb2World;
	import com.atishay.test.props.utils.Props;
	
	/**
	 * ...
	 * @author atjain
	 */
	public class DropperParams
	{
		public static const instance:DropperParams = new DropperParams();
		public var position:amPoint2d = new amPoint2d();
		public var genStep:Number = 0;
		public var useJitter:Boolean = true;
		public var useGenerationJitter:Boolean = true;
		private var _frequencyDict:Vector.<DropSpecs>;
		private var _totalFrequency:uint;
		private static var defaultTotalFrequency:uint = 0;
		
		[Embed(source="../../../../../assets/dropper.png")]
		private static var imageDropper:Class;
		public var actorBmp:Class = imageDropper;
		
		public function DropperParams():void
		{
			if (instance == null || instance.frequencyDict == null)
			{
				_frequencyDict = new Vector.<DropSpecs>();
				
				//Pushing the Default frequency values
				_frequencyDict.push(new DropSpecs(Props.RIGID_BALL, 4, null));
				_frequencyDict.push(new DropSpecs(Props.RIGID_BOX, 1, null));
				prepareFrequency();
				defaultTotalFrequency = _totalFrequency;
			}
			else
			{
				_totalFrequency = defaultTotalFrequency;
				_frequencyDict = instance.frequencyDict;
			}
		
		}
		
		public function resetViaXml(xml:XML):DropperParams
		{
			var x:Number = Number(xml.Position.X[0]);
			var y:Number = Number(xml.Position.Y[0]);
			try
			{
				var genStep:Number = Number(xml.CustomProperties.Property.(@Name == "Generation Step").string[0]);
			}
			catch (e:Error)
			{
				genStep = 1;
			}
			try
			{
				var useJitter:Boolean = xml.CustomProperties.Property.(@Name == "Use Jitter").boolean[0] == "true";
			}
			catch (e:Error)
			{
				useJitter = true;
			}
			try
			{
				var useGenerationJitter:Boolean = (xml.CustomProperties.Property.(@Name == "Generation Jitter").boolean[0]) == "true";
			}
			catch (e:Error)
			{
				useGenerationJitter = true;
			}
			
			try
			{
				var overrideFrequency:Boolean = (xml.CustomProperties.Property.(@Name == "Override Frequency").boolean[0]) == "true";
			}
			catch (e:Error)
			{
				overrideFrequency = false;
			}
			
			
			if (overrideFrequency)
			{
				var params:DropperParams = new DropperParams().reset(x, y, genStep, useJitter, useGenerationJitter);
				var frequencyDictionary:Vector.<DropSpecs> = new Vector.<DropSpecs>();
				try
				{
					var ballFrequency:Number = Number(xml.CustomProperties.Property.(@Name == "Ball Frequency").string[0]);
				}
				catch (e:Error)
				{
					ballFrequency = 0;
				}
				frequencyDictionary.push(new DropSpecs(Props.RIGID_BALL, ballFrequency));
				try
				{
					var boxFrequency:Number = Number(xml.CustomProperties.Property.(@Name == "Box Frequency").string[0]);
				}
				catch (e:Error)
				{
					boxFrequency = 0;
				}
				frequencyDictionary.push(new DropSpecs(Props.RIGID_BOX, boxFrequency));
				params.frequencyDict = frequencyDictionary;
				params.prepareFrequency();
				return params;
			}
			else
				return reset(x, y, genStep, useJitter, useGenerationJitter);
		}
		
		public function reset(x:Number, y:Number, genStep:Number = 1, useJitter:Boolean = true, useGenerationJitter:Boolean = true, actorBmp:Class = null):DropperParams
		{
			this.useJitter = useJitter;
			this.useGenerationJitter = useGenerationJitter;
			this.position = new amPoint2d(x, y);
			this.genStep = genStep;
			if (actorBmp == null)
				this.actorBmp = imageDropper;
			else
				this.actorBmp = actorBmp;
			return this;
		}
		
		public function get frequencyDict():Vector.<DropSpecs>
		{
			return _frequencyDict;
		}
		
		public function set frequencyDict(value:Vector.<DropSpecs>):void
		{
			if (this == instance)
			{
				trace("DropperParams :: Trying to Modify default frequency Dictionary");
				return;
			}
			_frequencyDict = value;
			prepareFrequency();
		}
		
		public function get totalFrequency():uint
		{
			return _totalFrequency;
		}
		
		private function prepareFrequency():void
		{
			if (instance != null && _frequencyDict == instance.frequencyDict)
				return;
			_totalFrequency = 0;
			for each (var spec:DropSpecs in _frequencyDict)
			{
				_totalFrequency += spec.frequency;
				spec.locationNum = totalFrequency;
			}
		}
	}

}