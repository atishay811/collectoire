package com.atishay.test.props.params 
{
	import As3Math.geo2d.amPoint2d;
	import com.atishay.test.interfaces.IDroppableParams;
	/**
	 * ...
	 * @author atjain
	 */
	public class RigidBallParams implements IDroppableParams
	{
		//Use instance for caching purposes.
		public static var instance:RigidBallParams = new RigidBallParams();
		
		[Embed(source="../../../../../assets/ball.png")]
		private static var imageCircle:Class;
		public var actorBmp:Class = imageCircle;
		private var _position:amPoint2d;
		public var radius:Number = 10;
		public var mass:Number = 1;
		public var hasFixedrotation:Boolean = true;
		public function reset(x:Number, y:Number, radius:Number = 10,mass:Number = 0, hasFixedrotation:Boolean = true, actorBmp:Class = null ) :RigidBallParams
		{
			this.position = new amPoint2d(x, y);
			this.radius = radius;
			this.mass = mass;
			this.hasFixedrotation = hasFixedrotation;
			if(actorBmp == null)
				this.actorBmp = imageCircle;
			else
				this.actorBmp = actorBmp;
			return this;
		}
		
		public function defaultValues():IDroppableParams
		{
			radius = 10;
			mass = 1;
			actorBmp = imageCircle;
			return this;
		}
		
		public function get position():amPoint2d 
		{
			return _position;
		}
		
		public function set position(value:amPoint2d):void 
		{
			_position = value;
		}
	}
}