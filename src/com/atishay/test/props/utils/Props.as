package com.atishay.test.props.utils 
{
	import com.atishay.test.*;
	import com.atishay.test.props.*;
	import com.atishay.test.props.droppables.*;
	/**
	 * ...
	 * @author atjain
	 */
	public final class Props 
	{
		public static const RIGID_BALL:String 		=	 	"rigidBall";
		public static const RIGID_BOX:String		=		"rigidBox";
		public static const DROPPER:String			=		"dropper";
		public static const MAGIC_HAT:String		=		"magicHat";
		public static const OBSTRUCTION_BOX:String	=		"box";
		public static function register():void
		{
			PropFactory.singleton.register(Dropper, Props.DROPPER);
			PropFactory.singleton.register(MagicHat, Props.MAGIC_HAT);
			PropFactory.singleton.register(ObstructionBox, OBSTRUCTION_BOX);
			PropFactory.singleton.register(RigidBox, Props.RIGID_BOX);
			PropFactory.singleton.register(RigidBall, Props.RIGID_BALL);
		}
	}

}