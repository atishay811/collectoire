package com.atishay.test.props.utils 
{
	/**
	 * ...
	 * @author atjain
	 */
	public class DropSpecs 
	{
		public var title:String;
		public var params:Object;
		public var frequency:uint;
		
		/*Overwritten by the DropperParams, for internal use only*/
		public var locationNum:uint = 0;
		
		public function DropSpecs(title:String,frequency:uint = 1 ,params:Object = null) 
		{
			this.title = title;
			this.frequency = frequency;
			this.params = params;
		}
		
	}

}