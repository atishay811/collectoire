package com.atishay.test.interfaces 
{
	
	/**
	 * ...
	 * @author atjain
	 */
	public interface IProp 
	{
		function set title(value:String):void;
		function get title():String;
		function init(params:Object):Boolean;
		function cleanUp():void
	}
	
}