package com.atishay.test.interfaces 
{
	import As3Math.geo2d.amPoint2d;
	
	/**
	 * ...
	 * @author atjain
	 */
	public interface IDroppable extends IProp
	{
		function get points():int;
		function set position(position:amPoint2d):void;
	}
	
}