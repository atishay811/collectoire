package com.atishay.test.interfaces 
{
	import As3Math.geo2d.amPoint2d;
	
	/**
	 * ...
	 * @author atjain
	 */
	public interface IDroppableParams 
	{
		function defaultValues():IDroppableParams;
		function get position():amPoint2d;
	}
	
}