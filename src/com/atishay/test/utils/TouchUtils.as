package com.atishay.test.utils 
{
	import com.atishay.test.Main;
	/**
	 * ...
	 * @author ...
	 */
	public class TouchUtils 
	{
		public static function get mouseX():Number
		{
			return (Main.GameSprite.stage.mouseX - Main.GameSprite.x) / Main.GameSprite.scaleX;
		}
		public static function get mouseY():Number
		{
			return (Main.GameSprite.stage.mouseY - Main.GameSprite.y) / Main.GameSprite.scaleY;
		}
		
	}

}