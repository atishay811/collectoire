package com.atishay.test.utils
{
	import flash.utils.ByteArray;
	import com.atishay.test.PropFactory;
	import com.atishay.test.Main;
	import QuickB2.objects.qb2Object;
	import com.atishay.test.Level;
	
	/**
	 * ...
	 * @author atjain
	 */
	public class LevelSetter
	{
		public static var instance:LevelSetter = new LevelSetter();
		[Embed(source="../../../../assets/Collectoria.xml",mimeType="application/octet-stream")]
		private static var levelXML:Class;
		public var numLevels:uint;
		private var layout:XML;
		public function LevelSetter():void
		{
		}
		public function init():LevelSetter
		{
			var file:ByteArray = new levelXML();
			var str:String = file.readUTFBytes(file.length);
			layout = new XML(str);
			numLevels = layout.Layers.Layer.length();
			return this;
		}
		
		public function prepareLevel(no:uint):void
		{
			if (no <1 || no > numLevels)
				throw(new Error("Level not present"));
			var levelXML:XML = layout.Layers.Layer[no - 1];
			var numItems:int = levelXML.Items.Item.length();
			var object:qb2Object;
			for each(var item:XML in levelXML.Items.Item)
			{
				if (item.asset_name[0] == "topLeft")
				{
					Level.instance.top = Number(item.Position.Y[0]);
					Level.instance.left = Number(item.Position.X[0]);
				}
				else if (item.asset_name[0] == "bottomRight")
				{
					Level.instance.bottom = Number(item.Position.Y[0]);
					Level.instance.right = Number(item.Position.X[0]);
				}
				else
				{
					object = PropFactory.singleton.create(item.asset_name[0], item) as qb2Object;
					if (object != null)
						Main.world.addObject(object);
				}
			}
			Main.world.addObject(Level.instance.init());
		}
	
	}

}