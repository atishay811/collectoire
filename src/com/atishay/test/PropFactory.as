package com.atishay.test 
{
	import com.atishay.test.interfaces.*;
	import flash.utils.*;
	import mx.utils.*;
	import QuickB2.objects.*;
	import QuickB2.objects.tangibles.*;
	/**
	 * ...
	 * @author ...
	 */
	public class PropFactory 
	{
		public static var singleton:PropFactory;
		private var activeObjects:Dictionary;
		private var inactiveObjects:Dictionary;
		public var objectTypes:Dictionary;
		private var world:qb2ObjectContainer;
		public function PropFactory(container:qb2ObjectContainer) 
		{
			world = container;
			super();
			if (singleton != null)
				throw("Re Instantiating Singleton: PropFactory");
			singleton = this;
			activeObjects = new Dictionary();
			inactiveObjects = new Dictionary();
			objectTypes = new Dictionary();
		}
		public function register(objectClass:Class,title:String):Boolean
		{
			if (objectTypes[title] != undefined)
				return false;
			if (describeType(objectClass).factory.implementsInterface.(@type == getQualifiedClassName(IProp)).length() == 0)
			{
				trace("PropFactory :: Class not IProp: " + objectClass);
				return false;	
			}
			objectTypes[title] = objectClass;
			activeObjects[title] = new LinkedList();
			inactiveObjects[title] = new LinkedList();
			return true;
		}
		public function create(title:String, params:Object = null):IProp
		{
			if (objectTypes[title] == undefined)
			{
				trace("PropFactory :: Creating unknown object: " + title);
				return null;
			}
			var newObj:IProp;
			if (inactiveObjects[title].length == 0)
			{
				newObj = new objectTypes[title];
				newObj.title = title;
			}
			else
			{
				newObj = inactiveObjects[title].pop().value;
			}
			prepare(newObj, params);
			//world.addObject(newObj);
			activeObjects[title].push(newObj);
			return newObj;
		}
		
		public function remove(prop:IProp):Boolean
		{
			activeObjects[prop.title].remove(prop);
			inactiveObjects[prop.title].push(prop);
			prop.cleanUp();
			if(prop is qb2Object)
			{
				var obj:qb2Object = prop as qb2Object;
				obj.removeFromParent();
			}
			return false;
		}
		
		private function prepare(obj:IProp, params:Object):void
		{
			if (obj.init(params) == true)
				return;
			for (var param:String in params)
			{
				try
				{
					if (params[param] == "true")
						obj[param] = true;
					else if (params[param] == "false")
						obj[param] = false;
					else
						obj[param] = params[param];
				}
				catch (e:Error)
				{
					trace("PropFactory :: The parameter " + param + " does not exist on " + obj);
				}
			}			
		}
	}
}